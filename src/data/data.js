export const pcard = [
  {
    img: "./img/youtube.jpg",
    alt: "YouTube",
    link: "https://yt-democopy.netlify.app/",
    cat: "React",
    title: "Copy of YouTube RapidAPI ",
    git: 'https://gitlab.com/group_tarashev/youtube',
    desc: 'I buld this copy of YouTube with ReactJS. All components are builded with Material UI tools, I used Redux to change the theme. The theme color is stored in Material UI theme scheme. ',
    tools: ["./svg/react-javascript-js-framework-facebook-svgrepo-com.svg", './svg/redux-svgrepo-com.svg', './svg/material-ui-svgrepo-com.svg', '/svg/api-svgrepo-com.svg']
  },
  {
    img: "./img/remote.jpg",
    alt: "Git Remote Job",
    link: "https://githubremote.netlify.app/",
    cat: "React",
    title: "Git Remote Job API",
    git: 'https://gitlab.com/group_tarashev/gitremotejob',
    desc: 'I accepted devChallenges.io to build job search web application. I used useContext in ReactJS, to fetch with axios the data and filtered it. You can searching jobs by country, title, category, remote or not',
    tools:['./svg/js-svgrepo-com.svg','./svg/react-javascript-js-framework-facebook-svgrepo-com.svg', "./svg/css-3-svgrepo-com.svg", './svg/api-svgrepo-com.svg']
  },
  {
    img: "./img/prompt.jpg",
    alt: "Prompt",
    link: "https://demo-prompts.netlify.app/",
    cat: "React",
    title: 'Prompt demo-copy of site',
    git: 'https://gitlab.com/group_tarashev/prompt',
    desc: "Prompt is a copy of promptbase site, Here I learn how to fetch enourmous JSON file, and loading prompts on scrolling. I use ReactJS, and React-Slick for sliding the prompts ",
    tools:['./svg/js-svgrepo-com.svg', "./svg/react-javascript-js-framework-facebook-svgrepo-com.svg", './svg/css-3-svgrepo-com.svg', "./svg/json.svg"]
  },
  {
    img: "./img/portfolio.png",
    alt: "Portfolio",
    link: "https://it-port.netlify.app",
    cat: "TypeScript",
    title:"Portfolio",
    git: 'https://gitlab.com/group_tarashev/it-port',
    desc: "This is one of my portfolio, with CSS amimation",
    tools: ['./svg/logo-ts-svgrepo-com.svg', './svg/css-3-svgrepo-com.svg']
  },
  {
    img: "./img/weather.jpg",
    alt: "Weather",
    link: "https://thunder-app-api.netlify.app/",
    cat: "TypeScript",
    title: "API Wheater app",
    git: 'https://gitlab.com/group_tarashev/thunder',
    desc: "With API Weather provider, i build weather app, where you can search by city. Onloading the app get you capital city for location. The search history is added to LocalStorage",
    tools:['./svg/logo-ts-svgrepo-com.svg','./svg/api-svgrepo-com.svg']
  },
  {
    img: "./img/upwork.jpg",
    alt: "Upwork",
    link: "https://demo-upwork.netlify.app/",
    cat: "React",
    title: "Demo-Copy of Upwork freelance platform",
    git: 'https://github.com/iliyan90/copy-upwork',
    desc: 'My first copy site upwork, where i learn a lot about ReactJS',
    tools: ['./svg/react-javascript-js-framework-facebook-svgrepo-com.svg','./svg/css-3-svgrepo-com.svg']
  },
  {
    img: "./img/crosword1.png",
    link: "https://www.youtube.com/watch?v=ZyQWJpVji78",
    alt: "Crosword",
    cat: "React-Native",
    title: "Crosword Android App",
    git: 'https://drive.google.com/file/d/1q7VahNec39WVIJKoiX9aN9gSmIe4C1xI/view',
    desc: 'It was good chalenge to build this app with React-Native. Where i used ternary search algorithm, to find random words. The level and pickles are saved in localstorage ',
    tools:['./svg/react-javascript-js-framework-facebook-svgrepo-com.svg','./svg/css-3-svgrepo-com.svg']
  },
  {
    img: "./img/tips.png",
    link: "https://gitlab.com/group_tarashev/tips-calculator",
    alt: "Tips",
    cat: "React-Native",
    title: "Tips Calculator Android App",
    git: 'https://gitlab.com/group_tarashev/tips-calculator',
    desc: 'Enter the bill, pick a procent, and calculate the amount you want give',
    tools:['./svg/react-javascript-js-framework-facebook-svgrepo-com.svg','./svg/css-3-svgrepo-com.svg']
  },
  {
    link: "https://app-task-list-1.netlify.app/",
    img: "./img/tasklist.png",
    alt: "Tasklist",
    cat: "TypeScript",
    title: "Task list stored in LocalStorage",
    git: 'https://gitlab.com/group_tarashev/task-list',
    desc: 'Task list that is keep the tasks in the local storage. You can change progress and it`s possible to edit the task',
    tools: ['./svg/logo-ts-svgrepo-com.svg','./svg/css-3-svgrepo-com.svg']
  },
];