import './styles/App.css'
import Nav from './sections/Nav'
import Hero from './sections/Hero'
import About from './sections/About'
import Expertice from './sections/Expertice'
import Journey from './sections/Journey'
import CardList from './sections/CardList'
import Portfolio from './sections/Portfolio'
import Project from './sections/Project'
import Touch from './sections/Touch'
import Footer from './sections/Footer'

function App() {
  
    
  return (
    <main className='App' >
      <Nav/>
      <Hero/>
      <About/>
      <Expertice/>
      <Journey/>
      <CardList/>
      <Portfolio/>
      <Project/>
      <Touch/>
      <Footer/>
    </main>
  )
}

export default App
