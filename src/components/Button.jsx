import React from "react";
import { scrollTo } from "../tool/scroll";

const Button = ({ color, bgcolor, children, border }) => {
  return (
    <div
      style={{
        color: color,
        backgroundColor: bgcolor,
        width: 160,
        height: 50,
        padding: 10,
        border: border,
        borderRadius: 40,
        display: 'flex',
        alignItems:'center',
        justifyContent:'center',
        fontWeight:'bold',
        cursor:'pointer',
        fontSize:18,
      }}
    >
      {children}
    </div>
  );
};

export default Button;
