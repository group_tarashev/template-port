import React from "react";
import Card from "./Card";
import "../styles/cards.css";
import { BsCodeSlash, BsAndroid } from "react-icons/bs";
import { BiCodeCurly } from "react-icons/bi";
const card = [
  {
    icon: <BsCodeSlash  size={22}/>,
    title: "Web Design",
    desc: "Responsive web design using HTML, CSS, JavaScript and libraries as MaterialUI, Grommet, or pure CSS",
  },
  {
    icon: <BiCodeCurly  size={22}/>,
    title: "Web Application",
    desc: "Available to contribute to open-source projects",
  },
  {
    icon: <BsAndroid size={22}/>,
    title: "Android App",
    desc: 'Entry skills in React-Native android application. I`ve developt two apps. Crosswords app and Tips calculator app'
  }
];

const CardList = () => {
  return (
    <section className="card-list" id="cards">
      <div className="content">
        <span className="title">Skills</span>
        <p className="cont-title">What I Offer</p>
      </div>
      <div className="cards">
        {card.map((item, i) => (
          <Card item={item} key={i} />
        ))}
      </div>
    </section>
  );
};

export default CardList;
