import React, { useEffect } from "react";
import "../styles/hero.css";
import { TbAward } from "react-icons/tb";
import { BiTask, BiSupport } from "react-icons/bi";
import { animeLang, animeText } from "../tool/animate";
const Hero = () => {
  useEffect(() => {
    animeText(); //animated ml2 h1
    animeLang(); //animated self
  }, []);
  return (
    <section className="hero" id="hero">
      <div className="hero-left">
        <p className="greet">Hi, I am </p>
        <h1 className="ml2">Iliyan Tarashev</h1>
        <p className="self">Junior Front-End Developer</p>
        <p className="desc">
          I am enthusiastic about creating engaging and responsive user
          interfaces. My dedication to continuous learning means I'm always
          eager to embrace new technologies and stay current with industry best
          practices. Let's build something extraordinary together!
        </p>
      </div>
      <div className="hero-rigth">
        <img src="./img/animated.png" alt="" />
      </div>
      <div className="some-about">
        <div className="year">
          <div className="icon">
            <TbAward />
          </div>
          <div className="cont">
            <p>2 Year Personal Projects</p>
            <span>Experience</span>
          </div>
        </div>
        <div className="project-some">
          <div className="icon">
            <BiTask />
          </div>
          <div className="cont">
            <p>10+ Projects</p>
            <span>completed</span>
          </div>
        </div>

        <div className="supp">
          <div className="icon">
            <BiSupport />
          </div>
          <div className="cont">
            <p>Support</p>
            <span>Online 24/7</span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
