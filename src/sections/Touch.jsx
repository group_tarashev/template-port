import React, { useRef, useState } from "react";
import "../styles/touch.css";
import { FaPhoneFlip } from "react-icons/fa6";
import { MdOutlineMail } from "react-icons/md";
import { CiLocationOn } from "react-icons/ci";
import emailjs from "@emailjs/browser";
const id = import.meta.env.VITE_APP_ID;
const key = import.meta.env.VITE_APP_KEY;
const tmp = import.meta.env.VITE_APP_TEMP;
const Touch = () => {
  const form = useRef();
  const [inputs, setInputs] = useState({
    name: "",
    email: "",
    subject: "",
    message: "",
  });
  //multiplay handlechanges
  const handleChange = (e) => {
    const { value, name } = e.target;

    let input = { [name]: value };
    setInputs({ ...inputs, ...input });
  };
  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm(id, tmp, form.current, key).then(
      (result) => {
        console.log(result.text);
      },
      (error) => {
        console.log(error.text);
      }
    );
    setInputs({
      name: "",
      email: "",
      subject: "",
      message: "",
    });
  };
  return (
    <section className="touch" id="contact">
      <div className="touch-title">
        <span>Conact Me</span>
        <p>Get in Touch</p>
      </div>
      <div className="touch-content">
        <div className="left-touch">
          <form ref={form} className="form" onSubmit={sendEmail}>
            <div className="touch-name">
              <input
                type="text"
                placeholder="Name *"
                name="name"
                onChange={handleChange}
                required={true}
                value={inputs.name}
              />
              <input
                type="email"
                placeholder="Email *"
                name="email"
                onChange={handleChange}
                value={inputs.email}
                required={true}
              />
            </div>
            <input
              type="text"
              placeholder="Subject"
              name="subject"
              onChange={handleChange}
              value={inputs.subject}
            />
            <textarea
              required={true}
              name="message"
              id="message"
              cols="10"
              rows="5"
              placeholder="Message *"
              onChange={handleChange}
              value={inputs.message}
            ></textarea>
            <button className="btn-send" type="submit">
              Send
            </button>
          </form>
        </div>
        <div className="right-touch">
          <div className="call">
            <div className="icon">
              <FaPhoneFlip color="white" size={20} />
            </div>
            <div className="number">
              <span>Call Me</span>
              <p>(+359 896 XXX XXX)</p>
            </div>
          </div>
          <div className="email">
            <div className="icon">
              <MdOutlineMail color="white" size={20} />
            </div>
            <div className="email-add">
              <span>E-mail</span>
              <p>iliyan.tarashev@gmail.com</p>
            </div>
          </div>
          <div className="location">
            <div className="icon">
              <CiLocationOn color="white" size={20} />
            </div>
            <div className="number">
              <span>Location</span>
              <p>Bulgaria</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Touch;
