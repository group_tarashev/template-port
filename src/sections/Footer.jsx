import React from 'react'
import '../styles/footer.css'
import { FaGitlab } from "react-icons/fa6";
import { FaLinkedin } from "react-icons/fa";


const Footer = () => {
  return (
    <footer className='footer'>
        <h3>T.</h3>
        <p>Develop the world as you want</p>
        <div className="fcont">
            <a className="icon" href='https://gitlab.com/group_tarashev' target='_blank' ><FaGitlab color='white' size={22}/></a>
            <a className="icon" href='www.linkedin.com/in/iliyan-tarashev-a7063b122' target='_blank'>
                <FaLinkedin color='white' size={22}/>
            </a>
        </div>
            <div className="year">
              <p>Copyright  &copy;  I.T. {new Date().getFullYear()}</p>
            </div>
    </footer>
  )
}

export default Footer