import React, { useEffect, useState } from 'react'
import '../styles/nav.css'
import useScroll, { scrollTo } from '../tool/scroll'
const li = ['Home','My Intro','Services','Portfolio','Contact']
const scrLi = ['hero', 'about', 'cards', 'portfolio', "contact"]
const Nav = () => {
    const [open, setOpen] = useState(false);
    open && (document.body.style.overflow = 'hidden');
    open || (document.body.style.overflow = 'auto');
    
    const scroll = useScroll();
    let offset = 0;
    if(scroll > 700 && scroll < 1200){
        offset = 1
    }else if(scroll > 1200 && scroll < 2800){
        offset = 2
    }else if(scroll > 2800 && scroll < 3600){
        offset = 3
    }else if(scroll > 3600 && scroll < 4800){
        offset = 4
    }

    const active = (id) =>{
        if(id === offset){
            return 'b-bottom active'
        }else{
            return ''
        }
    }
  return (
    <nav className='nav' >
        <div className="left">
            <div className="logo">
                <p>T.</p>
            </div>
        </div>
        <div className="right">
            <ul className={open ? 'ul-nav' : 'ul-nav-close'}>
                {li.map((item, i) =>(
                    <li className={active(i)} onClick={() => {setOpen(false);
                        scrollTo(scrLi[i])
                    }} key={i}>{item} </li>
                ))}
            </ul>
            <div className={open? "menu-mb-close":"menu-mb"} onClick={() => setOpen(!open)}>
                <span></span>
            </div>
        </div>
    </nav>
  )
}

export default Nav