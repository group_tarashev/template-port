import React from "react";
import "../styles/journey.css";
import { PiStudentFill } from "react-icons/pi";
import { FaSuitcase } from "react-icons/fa";

const Journey = () => {
  const li = (item, i) => {
    return (
      <li key={i}>
        <div className="edu-cont">
          <p>{item.title}</p>
          <span className="edu-prog">{item.prog}</span>
          <span className="edu-time">{item.time}</span>
        </div>
      </li>
    );
  };

  return (
    <section className="journey">
      <div className="title-jour">
        <p>My Qualification</p>
        <span>Awesome Journey</span>
      </div>
      <div className="content-jour">
        <div className="left-jour">
          <div className="h3">
            <span>
              <PiStudentFill size={25} />
            </span>
            Education
          </div>
          <ul className="edu-ul">
            {educ.map((item, i) => {
              return li(item, i);
            })}
          </ul>
        </div>
        <div className="right-jour">
          <div className="h3">
            <span>
              <FaSuitcase size={25} />
            </span>{" "}
            Experience
          </div>
          <ul className="edu-ul">
            {exp.map((item, i) => {
              return li(item, i);
            })}
          </ul>
        </div>
      </div>
    </section>
  );
};

export default Journey;

const educ = [
  {
    title: "Self-study",
    prog: "HTML, CSS, JS, React, TS, Node, MySQL, XAMPP",
    time: "2021 - 2023",
  },
  {
    title: "Computer technology",
    prog: "Profesional Technical School ",
    time: "2005 - 2009",
  },
];
const exp = [
  {
    title: "Personal Projects",
    prog: "Web Development",
    time: "2021 - 2023",
  },
  
];
