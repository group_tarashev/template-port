import React from "react";
import "../styles/expertice.css"
const exp = [
    {
        title: "HTML",
        percent: 60,
    },
    {
        title: "React",
        percent: 30,
    },
    {
        title: "CSS",
        percent: 20,
    },
    {
        title: "JavaScript",
        percent: 20
    },
];
const exp1 = [
    {
        title:"TypeScript",
        percent: 20
    },
    {
        title: "Redux",
        percent: 20,
    },
    {
        title: "MaterialUI",
        percent: 40,
    },
    {
        title: "Grommet",
        percent: 30,
    },

]
const images = [
    './svg/react-javascript-js-framework-facebook-svgrepo-com.svg',
    './svg/html-svgrepo-com.svg',
    './svg/css-3-svgrepo-com.svg',
    './svg/js-svgrepo-com.svg',
    './svg/redux-svgrepo-com.svg',
    './svg/logo-ts-svgrepo-com.svg',
    './svg/bootstrap-svgrepo-com.svg',
    './svg/grommet-svgrepo-com.svg',
    "./svg/xampp-svgrepo-com.svg",
    "./svg/code-svgrepo-com.svg"
]

const Expertice = () => {
  return (
    <section className="expertice">
      <div className="title-exp">
        <p>Why Choose Me</p>
        <span>My Expertise area</span>
      </div>
      <div className="exp-image">
        {
            images.map((item, i) =>{
                return(
                    <img className={`img${i}`} src={item} key={i}/>
                )
            })
        }
      </div>
      {/* <div className="cont-exp">
        <div className="left-exp">
            <ul className="pro-list">
                {exp.map((item, i) =>(
                    <li className="pro-bar" key={i}>
                        <p>{item.title}</p>
                        <span className="full"></span>
                        <span className="pro" style={{width: `${item.percent}%`}}></span>
                        <span className="txt-perc">{item.percent}%</span>
                    </li>
                ))}
            </ul>
        </div>
        <div className="right-exp">
        <ul className="pro-list">
                {exp1.map((item, i) =>(
                    <li className="pro-bar" key={i}>
                        <p>{item.title}</p>
                        <span className="full"></span>
                        <span className="pro" style={{width: `${item.percent}%`}}></span>
                        <span className="txt-perc">{item.percent}%</span>
                    </li>
                ))}
            </ul>
        </div>
      </div> */}
    </section>
  );
};

export default Expertice;
