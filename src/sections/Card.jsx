import React from 'react'

const Card = ({item}) => {
  return (
    <div className='card'>
        <div className="icon">{item.icon}</div>
        <div className="content">
            <span>{item.title}</span>
            <p>{item.desc}</p>
        </div>
    </div>
  )
}

export default Card