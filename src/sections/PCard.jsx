import React, { useState } from "react";
import "../styles/portfolio.css";
import ModalImg from "./ModalImg";
import { FaGitlab } from "react-icons/fa6";
import { IoMdLink } from "react-icons/io";
const PCard = ({ id, cards, handleOpen, item, handleClose, open }) => {
  const isExist = cards[id];
  const [lookId, setLookId] = useState(id);
  return (
    <div className="image" onClick={() => setLookId(id)}>
      <a onClick={() => handleOpen(id)}>
        <img src={item.img} alt={item.alt} />
      </a>
      {open.isNum === id && (
        <ModalImg open={open} handleClose={handleClose}>
          <img className="image-mod scale-up-center" src={cards[lookId].img} alt="" />
          <div className="links">
            <p>{item.title}</p>
            <ul className="mod-ul">
              {item.tools.map((el, i) => (
                <li key={i} className="scale-up-center">
                    <img className="mod-img" src={el} alt="" />
                  </li>
              ))}
            </ul>
            <p className="scale-up-tl">{item.desc}</p>
            <div className="links-site">
              <div className="git">
                <a href={item.git} target="_blank">
                  <FaGitlab size={24} color="white" />
                </a>
              </div>
              <div className="site">
                <a href={item.link} target="_blank">
                  <IoMdLink size={24} color="white" />
                </a>
              </div>
            </div>
          </div>
        </ModalImg>
      )}
    </div>
  );
};

export default PCard;
