import React from "react";
import "../styles/project.css"
const Project = () => {
  return (
    <section className="project">
      <h3>Have a Project on Your Mind</h3>
      <p>
        You have an idea share with me. I`ll do my best, to make your project special. 
      </p>
    </section>
  );
};

export default Project;
