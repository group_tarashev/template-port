import React, { useState } from "react";
import "../styles/portfolio.css";
import PCard from "./PCard";
import ModalImg from "./ModalImg";
import { pcard } from "../data/data";
import useSize from "../tool/size";
const tabs = ["All", "React", "TypeScript", "React-Native"];

const lang = ["All", "React", "TypeScript", "React-Native"];
const Portfolio = () => {
  const [cards, setCards] = useState(pcard);
  const [open, setOpen] = useState({
    isOpen: false,
    inNum: 0
});
  const [isActive, setIsActive] = useState({
    num: 0,
  });
  const active = (i) => {
    if (i === isActive.num) {
      return "li-active";
    } else {
      return "li";
    }
  };
  const handleCards = (i) => {
    const isExist = pcard.filter((item) => item.cat === lang[i]);
    if (isExist.length !== 0) {
      setCards(isExist);
    } else {
      setCards(pcard);
    }
  };

  const handleOpen = (i) => {
    console.log(i);
    setOpen(prev=>({...prev, isOpen: true, isNum : i}));
  };
  const handleClose = () => {
    setOpen(prev=>({...prev, isOpen: false}));
  };
  return (
    <section className="portfolio" id="portfolio">
      <div className="port-titles">
        <span>My Portfolio</span>
        <p>Recent Personal Projects</p>
      </div>
      <div className="port-tabs">
        <div className="tabs">
          <ul>
            {tabs.map((item, i) => (
              <li
                onClick={() => {
                  setIsActive((prev) => ({ ...prev, num: i }));
                  handleCards(i);
                }}
                className={active(i)}
                key={i}
              >
                {item}
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div className="port-content">
        {cards.map((item, i) => (
          <PCard
            handleOpen={handleOpen}
            key={i}
            id={i}
            item={item}
            handleClose={handleClose}
            open={open}
            cards={cards}
            
          />
        ))}
      </div>
    </section>
  );
};

export default Portfolio;
