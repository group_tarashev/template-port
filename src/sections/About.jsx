import React from "react";
import "../styles/about.css";
import {
  BsPerson,
  BsHeadphones,
  BsFillBuildingFill,
  BsFileCode,
} from "react-icons/bs";
import { FaPhone } from "react-icons/fa";
import { AiOutlineMail } from "react-icons/ai";
import { TbMovie } from "react-icons/tb";
import { HiPhotograph } from "react-icons/hi";
export const contact = [
  {
    icons: <BsPerson color="#4287f5" size={20} />,
    title: "Name",
    titleAns: "Iliyan Tarashev",
  },
  {
    icons: <FaPhone color="#4287f5" size={20} />,
    title: "Phone",
    titleAns: "(+359 896 XXX XXX)",
  },
  {
    icons: <AiOutlineMail color="#4287f5" size={20} />,
    title: "Emial",
    titleAns: "iliyan.tarashev@gmail.com",
  },
];
export const inter = [
  {
    icon: <BsFileCode size={20} color="#4287f5" />,
    title: "Coding",
  },
  {
    icon: <BsHeadphones size={20} color="#4287f5" />,
    title: "Music",
  },
  {
    icon: <BsFillBuildingFill size={20} color="#4287f5" />,
    title: "Travel",
  },
  {
    icon: <TbMovie size={20} color="#4287f5" />,
    title: "Movie",
  },
];
const About = () => {
  return (
    <section className="about" id="about">
      <div className="left-about">
        <img src="./img/animated.png" alt="" />
      </div>
      <div className="right-about">
        <div className="intro">
          <h4>My Intro</h4>
          <span>About Me</span>
          <p>
            Aspiring and detail-oriented Junior Front-End Developer with a
            passion for crafting seamless and visually appealing user
            experiences. Armed with a solid foundation in HTML, CSS, and
            JavaScript, I am eager to contribute my skills to a dynamic
            development team. With a collaborative mindset and a love for
            solving challenges, I am ready to embark on a journey of continuous
            learning and growth in the world of web development.
          </p>
        </div>
        <div className="about-cont">
          {contact.map((item, i) => (
            <div key={i} className="name">
              <div className="name-icon">
                <span>{item.icons}</span>
                <p>{item.title} : </p>
              </div>
              <p className="name-text">{item.titleAns}</p>
            </div>
          ))}
        </div>
        <div className="interest">
          <span className="int-title">My interest</span>
          <div className="categ">
            {inter.map((item, i) => (
              <div key={i} className="title-int">
                {item.icon}
                <span>{item.title}</span>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
