import React from "react";
import { Box, Modal } from "@mui/material";
import useSize from "../tool/size";

const ModalImg = ({children, open, handleClose}) => {
  const size = useSize();
  return (
    <Modal
      open={open.isOpen}
      onClose={handleClose}
      aria-labelledby="modal-modal-project"
      aria-describedby="modal-modal-projects"
      closeAfterTransition={true}
      className="scale-up-ver-top"
    >
      <Box sx={size.x < 500 ? styleMd :style}>
        {children}
      </Box>
    </Modal>
  );
};

export default ModalImg;
const styleMd = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: "90%",
  height: 500,
  display:"flex",
  alignItems: 'center',
  // justifyContent: 'center',
  flexDirection: 'column',
  overflow: 'hidden',
  bgcolor: 'background.paper',
  boxShadow: 24,
  objecFit:'cover',
  outline: 'none',
  p: 4,
}
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: 500,
    // maxHeight: 500,
    display:"flex",
    alignItems: 'center',
    // justifyContent: 'center',
    flexDirection: 'column',
    overflow: 'hidden',
    height: 'auto',
    bgcolor: 'background.paper',
    boxShadow: 24,
    objecFit:'cover',
    outline: 'none',
    p: 4,
  };