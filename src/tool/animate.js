export const animeText = () => {
  var textWrapper = document.querySelector(".ml2");
  textWrapper.innerHTML =
    textWrapper &&
    textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

  anime
    .timeline({ loop: true })
    .add({
      targets: ".ml2 .letter",
      scale: [3, 1],
      opacity: [0, 1],
      translateZ: 0,
      easing: "easeOutExpo",
      duration: 950,
      delay: (el, i) => 70 * i,
    })
    .add({
      targets: ".ml2",
      opacity: 1,
      duration: 1000,
      easing: "easeOutExpo",
      delay: 1000,
    });
};

export const animeLang = () =>{
    let langText = document.querySelector('.self');
    langText.innerHTML = langText.textContent.replace(/\S/g, "<span class='langLetter'>$&</span>");
    anime.timeline({loop: true})
  .add({
    targets: '.self .langLetter',
    translateY: [100,0],
    translateZ: 0,
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 1400,
    delay: (el, i) => 300 + 30 * i
  }).add({
    targets: '.self .langLetter',
    translateY: [0,-100],
    opacity: [1,0],
    easing: "easeInExpo",
    duration: 1200,
    delay: (el, i) => 100 + 30 * i
  });
}
