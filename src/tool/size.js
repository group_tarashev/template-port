import React, { useEffect, useState } from 'react'

const useSize = () => {
    const [size, setSize] = useState({
        x: window.innerWidth,
        y: window.innerHeight
    })
    useEffect(()=>{
        const getSize = () =>{
            setSize({
                x:window.innerWidth,
                y:window.innerHeight
            })
        }
        document.addEventListener('resize', getSize())
        getSize();
        return () => document.removeEventListener('resize',getSize())
    },[size])
  return size
}

export default useSize