import { useEffect, useState } from "react"

export const  scrollTo  = (id) =>{
    const element = document.getElementById(id)
    element.scrollIntoView({
        behavior: 'smooth'
    })
}
const useScroll = () =>{
    const [scrllY, setScrllY] = useState(window.scrollY)
    useEffect(()=>{
        const getScroll = () =>{
            setScrllY(window.scrollY);
        }
        window.addEventListener('scroll', getScroll);
        return () => window.removeEventListener('scroll', getScroll)
    },[])
    return scrllY
}

export default useScroll